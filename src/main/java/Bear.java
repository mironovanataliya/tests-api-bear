import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Bear {
   private String bear_name;
   private BearType bear_type;
   private Integer bear_age;
   private String bear_id;

    public String getBear_id() {
        return bear_id;
    }

    public void setBear_id(String bear_id) {
        this.bear_id = bear_id;
    }

    public Bear(String bear_name, BearType bear_type, Integer bear_age) {
        this.bear_name = bear_name;
        this.bear_type = bear_type;
        this.bear_age = bear_age;
    }

    public Bear() { }

    public String getBear_name() {
        return bear_name;
    }

    public void setBear_name(String bear_name) {
        this.bear_name = bear_name;
    }

    public BearType getBear_type() {
        return bear_type;
    }

    public void setBear_type(BearType bear_type) {
        this.bear_type = bear_type;
    }

    public Integer getBear_age() {
        return bear_age;
    }

    public void setBear_age(Integer bear_age) {
        this.bear_age = bear_age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bear bear = (Bear) o;
        return Objects.equals(bear_name, bear.bear_name) &&
                bear_type == bear.bear_type &&
                Objects.equals(bear_age, bear.bear_age);
    }

    @Override
    public int hashCode() {

        return Objects.hash(bear_name, bear_type, bear_age);
    }

    @Override
    public String toString() {
        return "Bear{" +
                "bear_name='" + bear_name + '\'' +
                ", bear_type=" + bear_type +
                ", bear_age=" + bear_age +
                '}';
    }
}
