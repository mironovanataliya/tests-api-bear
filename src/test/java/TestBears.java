
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.hamcrest.Matchers;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


public class TestBears {

    private static final String BASE_URL = "http://localhost:8091/";
    private ObjectMapper mapper = new ObjectMapper();

    /**
     * Предварительно заполняем коллекцию 3мя сущностями
     *
     * @throws IOException
     */
    @BeforeClass
    public static void createListOfBears() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        HttpPost request = new HttpPost(BASE_URL + "bear");
        for (int i = 0; i < 4; i++) {
            StringEntity entity = new StringEntity(mapper.writeValueAsString(new Bear("Ivan", BearType.BLACK, 10)));
            request.setEntity(entity);
            HttpClientBuilder.create().build().execute(request);
        }

    }

    /**
     * Получаем код 200 при успешном существующим URL
     *
     * @throws IOException
     */
    @Test
    public void getting200code() throws IOException {
        HttpUriRequest getRequest = new HttpGet(BASE_URL + "bear");
        assertThat(HttpClientBuilder.create().build().execute(getRequest).getStatusLine().getStatusCode(), is(200));
    }

    /**
     * Получаем код 400 при несуществующей URL
     *
     * @throws IOException
     */
    @Test
    public void getting404code() throws IOException {
        HttpUriRequest getRequest = new HttpGet(BASE_URL + "404");
        assertThat(HttpClientBuilder.create().build().execute(getRequest).getStatusLine().getStatusCode(), is(404));
    }

    /**
     * Получаем сущность "EMPTY"(пусто) при несуществующем объекте
     *
     * @throws IOException
     */
    @Test
    public void givenBearDoesNotExists_whenBearInfoIsRetrieved_thenEmptyIsReceived()
            throws IOException {

        // Given
        String name = RandomStringUtils.randomAlphabetic(8);
        HttpUriRequest getRequest = new HttpGet(BASE_URL + "bear/" + name);

        // When
        String body = EntityUtils.toString(HttpClientBuilder.create().build().execute(getRequest).getEntity());
        // Then
        assertThat(
                body,
                equalTo("EMPTY"));
    }

    /**
     * POST
     * Создаем и отправляем сущность на сервер. Используется objectMapper который преобразует класс Bear в JSON
     * Полученый JSON в виде сущности задается для ПОСТ-запроса и отправляется на сервер.
     * Проверяем что сервер ответил 200, проверяем что массив объектов увеличился
     *
     * @throws IOException
     */
    @Test
    public void
    sendingNewBear_whenBearIsCreated_thenRetrieveCode200() throws IOException {

        // Given
        HttpUriRequest getRequest = new HttpGet(BASE_URL + "bear");
        Bear[] oldResource = RetrieveUtil.retrieveResourceFromResponse(
                HttpClientBuilder.create().build().execute(getRequest), Bear[].class);
        Bear bear = new Bear("postNewBear", BearType.BLACK, 10);

        HttpPost request = new HttpPost(BASE_URL + "bear");
        StringEntity entity = new StringEntity(mapper.writeValueAsString(bear));
        request.setEntity(entity);
        // When
        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        //Then

        HttpResponse getResponse = HttpClientBuilder.create().build().execute(getRequest);
        Bear[] resource = RetrieveUtil.retrieveResourceFromResponse(
                getResponse, Bear[].class);
        assertThat(response.getStatusLine().getStatusCode(), is(200));
        assertTrue(oldResource.length < resource.length);
    }

    /**
     * GET
     * Получаем объект из массива объектов с помощью утилитарного класса RetrieveUtil
     * Получаем у объекта ID, отправляем ГЕТ-запрос с полученным ID и получаем сущность.
     * Проверяем ответил ли сервер 200, проверяем что у полученной сущности и у предыдущего объекта ИМЯ совпадает
     *
     * @throws IOException
     */
    @Test
    public void
    givenBearExists_whenBearInformationIsRetrieved_thenRetrievedResourceIsCorrect()
            throws IOException {

        // Given
        HttpUriRequest getRequest = new HttpGet(BASE_URL + "bear");
        Bear[] resource = RetrieveUtil.retrieveResourceFromResponse(
                HttpClientBuilder.create().build().execute(getRequest), Bear[].class);

        // When
        Bear bear = resource[0];
        HttpGet request = new HttpGet(BASE_URL + "bear/" + bear.getBear_id());

        // Then
        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        Bear newBear = RetrieveUtil.retrieveResourceFromResponse(
                response, Bear.class);
        assertThat(response.getStatusLine().getStatusCode(), is(200));
        assertThat(bear.getBear_name(), Matchers.is(newBear.getBear_name()));
    }

    /**
     * PUT
     * Получаем объект из массива объектов с помощью утилитарного класса RetrieveUtil
     * Меняем у объекта имя и отпрвляем его обратно на сервер
     * Убеждаемся что сервер ответил ОК и код 200, убеждаемся что записалось новое имя
     *
     * @throws IOException
     */
    @Test
    public void
    putBear_whenBearIsPutted_thenRetrieveOK() throws IOException {

        // Given
        HttpUriRequest getRequest = new HttpGet(BASE_URL + "bear");
        HttpResponse getResponse = HttpClientBuilder.create().build().execute(getRequest);
        Bear[] resource = RetrieveUtil.retrieveResourceFromResponse(
                getResponse, Bear[].class);
        Bear bear = resource[0];
        bear.setBear_name("newIvan");
        HttpPut request = new HttpPut(BASE_URL + "bear/" + bear.getBear_id());
        request.setEntity(new StringEntity(mapper.writeValueAsString(bear)));
        // When
        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        //Then
        assertThat("newIvan", Matchers.is(resource[0].getBear_name()));
        assertThat(response.getStatusLine().getStatusCode(), is(200));
        assertThat(EntityUtils.toString(response.getEntity()), is("OK"));
    }

    /**
     * DELETE
     * Получаем объект из массива объектов с помощью утилитарного класса RetrieveUtil
     * Отправляем HttpDelete на сервер с ID этого объекта, убеждаемся что сервер ответил 200 и ОК
     *
     * @throws IOException
     */
    @Test
    public void
    deleteBear_whenBearIsDeleted_thenRetrieve200() throws IOException {

        // Given
        HttpUriRequest getRequest = new HttpGet(BASE_URL + "bear");
        HttpResponse getResponse = HttpClientBuilder.create().build().execute(getRequest);
        Bear[] resource = RetrieveUtil.retrieveResourceFromResponse(
                getResponse, Bear[].class);
        Bear newBear = resource[0];
        HttpUriRequest request = new HttpDelete(BASE_URL + "bear/" + newBear.getBear_id());
        // When
        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        // Then
        assertThat(response.getStatusLine().getStatusCode(), is(200));
        assertThat(EntityUtils.toString(response.getEntity()), is("OK"));
    }
}
